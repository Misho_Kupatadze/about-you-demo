const Helpers = {
    /**
     * Keyword search in product. Search targets: Name, Categories, Brand Name
     * @param  {String} searchQuery   - Search keyword
     * @param  {Object} product       - Target product
     * @return {Boolean}              - Search result (true = product contains keyword)
     */
    searchInProduct (searchQuery, product) {
        const searchTargets =
            [product.name, product.brandName, ...product.categories]
                .filter(s => !!s)

        return !!searchTargets.find(s => s.toUpperCase().includes(searchQuery.toUpperCase()))
    },

    /**
     * Check if product price jumps in price range
     * @param  {Object} priceRange    - Min:Max values of price range
     * @param  {Number} productPrice  - Actual price of the product
     * @return {Boolean}              - Comparison result (true = price jumps in range)
     */
    filterByPrice (priceRange, productPrice) {
        // if priceRange is not set, all products are valid
        if(!priceRange || !priceRange.min) return true

        return priceRange.min <= productPrice && productPrice <= priceRange.max
    },

    /**
     * Remove Object properties with empty/undefined/null values
     * @param  {Object} obj  - target Object
     * @return {Object}      - Result object
     */
    clean(obj) {
        for (const propName in obj) {
            if (!(obj[propName])) delete obj[propName]
        }
        return {...obj}
    },

    /**
     * Compare objects on key-value level (no respect to property ordering)
     * @param  {Object} object1  - First Object
     * @param  {Object} object2  - Second Object
     * @return {Boolean}         - Comparison result (true = objects are equal)
     */
    shallowEqual(object1, object2) {
        const keys1 = Object.keys(object1);
        const keys2 = Object.keys(object2);

        if (keys1.length !== keys2.length) {
            return false;
        }

        for (let key of keys1) {
            if (object1[key] !== object2[key]) {
                return false;
            }
        }

        return true;
    }
}

export default Helpers