export default {
    apiBaseUrl: 'https://m.aboutyou.de/api/products/v2',
    imageBaseUrl: 'http://cdn.aboutstatic.com/file',
    productsPerPage: 20
}