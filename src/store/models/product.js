import { Model } from '@vuex-orm/core'
import Brand from "@/store/models/brand";
import Helpers from "@/utils/helpers";
import Api from "@/api";
import Variables from '@/utils/variables'

export default class Product extends Model {
    static entity = 'products'

    static fetchAllUrl = () => 'getProducts'
    static fetchDetailsUrl = (productId) => `getProducts?productId=${productId}`

    static fields () {
        return {
            id: this.attr(null),
            name: this.attr(''),
            image: this.attr({}),
            brand: this.belongsTo(Brand, 'brand_id'),
            brandName: this.attr(''),
            brand_id: this.attr(null),
            categories: this.attr([]),
            old_price: this.attr(false),
        }
    }

    // additional field for formatted price
    get price () {
        return this.old_price ? this.old_price / 100 : ''
    }

    static mutators () {
        return {
            // modify default image object and return absolute string instead
            image (value) {
                if(!value?.hash) return value
                return `${Variables.imageBaseUrl}/${value.hash}`
            }
        }
    }

    static async fetchAll () {
        const res = await Api.get(this.fetchAllUrl())

        if(!res.data) return

        await this.save(res.data)
    }

    static async fetchDetails (productId) {
        const res = await Api.get(this.fetchDetailsUrl(productId))

        if(!res.data) return

        await this.save(res.data)
    }

    // Preprocess and save data in store
    static async save(data) {
        const processedData = this.preProcess(data)
        await this.insertOrUpdate({data: processedData})
    }

    // add missing properties to build relation between Product-Brand
    static preProcess (apiResponse) {
        return apiResponse.map(s => {
            return {...s.product,
                brand_id: s.product.brand.id,
                brandName: s.product.brand.name
            }
        })
    }

    // base query to get products from store
    static baseQuery = (brand, searchQuery, priceRange) => {
        return Product
            .query()
            .whereHas('brand', (query) => {
                return (!brand || query.where('id', brand))
            })
            .with('brand')
            .where((product) => {
                return !searchQuery || Helpers.searchInProduct(searchQuery, product)
            })
            .where('price', (value) => {
                return !priceRange || Helpers.filterByPrice(priceRange, value)
            })
    }
}