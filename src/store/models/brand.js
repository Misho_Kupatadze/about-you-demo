import { Model } from '@vuex-orm/core'
import Product from "@/store/models/product";
import Helpers from "@/utils/helpers";

export default class Brand extends Model {
    static entity = 'brands'

    static fields () {
        return {
            id: this.attr(null),
            name: this.attr(''),
            products: this.hasMany(Product, 'brand_id')
        }
    }

    // additional getter for select
    get text () {
        return this.name
    }

    // additional getter for select
    get value () {
        return this.id
    }

    // base query to get brands from store
    static getBrands (searchQuery, priceRange) {
        return this.query()
        .with('products')
        .whereHas('products', (query) => {
            return !searchQuery || query.where((product) => Helpers.searchInProduct(searchQuery, product))
        })
        .whereHas('products', (query) => {
            return query.where('price', (price) => {
                return !priceRange || Helpers.filterByPrice(priceRange, price)
            })
        })
        .get()
    }
}