import Vue from 'vue'
import Vuex from 'vuex'
import VuexORM from '@vuex-orm/core'
import Product from '@/store/models/product'
import Brand from "@/store/models/brand";

Vue.use(Vuex)

const database = new VuexORM.Database()

database.register(Product)
database.register(Brand)

// Create Vuex Store and register database through Vuex ORM.
const store = new Vuex.Store({
    plugins: [VuexORM.install(database)]
})

export default store