import axios from "axios";
import Vue from 'vue'
import Variables from '@/utils/variables'

const baseUrl = Variables.apiBaseUrl

const Api = {
    async get (url) {
        try {
            return await axios.get(`${baseUrl}/${url}`)
        }
        catch (e) {
            // display error message if Api call fails
            const vm = new Vue()
            vm.$bvToast.toast(`${e.message}`, {
                title: 'Error',
                variant: 'danger',
                solid: true
            })
        }
    }
}

export default Api