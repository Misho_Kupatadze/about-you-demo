import Vue from "vue"

Vue.filter("price", function(price) {
    if(!price) return ''

    const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'EUR',
        minimumFractionDigits: 2
    })

    return formatter.format(price)
})