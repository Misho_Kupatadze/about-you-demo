import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "@/store";
import "./filters";

// Import Bootstrap
import { BootstrapVue, IconsPlugin, BToast } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false;

// Use Bootstrap
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.component('b-toast', BToast) // To display notification if Api call fails


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
