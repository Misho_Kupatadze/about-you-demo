import Vue from "vue";
import VueRouter from "vue-router";
import Details from "@/views/Details";
import Home from "@/views/Home";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/details/:id",
    name: "details",
    component: Details
  }
];

const router = new VueRouter({
  routes
});

export default router;
