# About You - Technical assignment

[Live Demo](https://about-you-demo.netlify.app)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Packages used:
* _Vuex ORM_ - To simplify store access and data management,  
* _axios_ - To have more control on Api calls,
* _bootstrap_ - Basic UI,
* _veeno_ - Custom range slider with two values (price selector).


### Roadmap: 
Application contains two pages - product listing and product details. 
Main functionality is on product listing, where users can use dynamic filter, sort and pagination.

### Product filter:
Product filter contains 3 fields and changing one (of those fields), 
will dynamically update others (if necessary).
Examples:


* Let's assume searching for keyword "shirt" results in X products. 
Other two fields (brand, price) will automatically adjust to those X products only. 
Therefore, price range will take min/max ranges from X products and brand select only keeps brands for X products.

* Let's assume selecting specific brand results in X products. 
Price field will automatically take min/max values from X products only. 

* Let's assume selecting min/max prices results in X products. 
Brand select will update and only show brands from those X Products only. 

Selected filters are shown as tags, and they are removed/cancelled on click. 

 
### Sort:
Default sort is alphabetic, based on product name. 
Switching to non-default values sorts products based on price (asc/desc).

### Search:
Search is not case-sensitive, keywoards are searched in product name, brand name, category name. 
Search input has debounce 500ms to provide better/stable UX.  

### Price selector:
Price selector takes min/max values from the filtered products. 
It will hide if min/max prices of filtered products are equal. 
This happens when search and/or brand filter results in one product (or multiple products with same price).

### Store management:
_Current store management strategy is chosen since Api doesn't support pagination/keyword search/brand selection._

Data is fetched and kept in store on product list page. 
Filtering/sorting/pagination - all happens based in Store using Vuex ORM. 
On product details page, product is only refetched if it's not already in store.
Each product object is pre-processed before saving in store to 
support Product-Brand relation.

Please note: _Product list from API is changing often, 
it might lead to weird behaviour of product page_ 